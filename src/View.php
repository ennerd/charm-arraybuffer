<?php
namespace Charm\ArrayBuffer;

use FFI, IteratorAggregate, Countable, ArrayAccess;

/**
 * This class presents a view in the host endianness.
 */
class View implements IteratorAggregate, Countable, ArrayAccess {
    protected $view;
    protected $length;
    protected $elementSize;
    protected $sample;

    public function __construct(FFI\CData $buffer, FFI\CType $type) {
        $this->elementSize = FFI::sizeof($type);
        $this->length = intval(FFI::sizeof($buffer) / $this->elementSize);
        $this->view = FFI::cast(FFI::arrayType($type, [ $this->length ]), $buffer);
        $this->sample = FFI::new($type);
        if (is_int($this->sample->cdata)) {
            $this->sample->cdata = -1;
        }
    }

    /**
     * Flips the endianness from big endian to little endian
     * and vice versa.
     */
    public function flip($value) {
        if (is_int($this->sample->cdata)) {
            /* Not sure if we need a different algorithm for signed ints
            if ($this->sample->cdata < 0) {
                die("SIGNED INTEGER, NEED DIFFERENT ALG");
            }
            */
            switch ($this->elementSize) {
                case 1:
                    return $value;
                case 2:
                    return
                        $value << 8 & 0xFF00 |
                        $value >> 8 & 0x00FF;
                case 4:
                    return
                        $value << 24 & 0xFF000000 |
                        $value << 8 & 0x00FF0000 |
                        $value >> 8 & 0x0000FF00 |
                        $value >> 24 & 0x000000FF;
                case 8:
                    return
                        (int) $value << 56 & 0xFF00000000000000 |
                        (int) $value << 40 & 0x00FF000000000000 |
                        (int) $value << 24 & 0x0000FF0000000000 |
                        (int) $value << 8 &  0x000000FF00000000 |
                        (int) $value >> 8 &  0x00000000FF000000 |
                        (int) $value >> 24 & 0x0000000000FF0000 |
                        (int) $value >> 40 & 0x000000000000FF00 |
                        (int) $value >> 56 & 0x00000000000000FF;
            }
        } elseif (is_float($this->sample->cdata)) {
            switch ($this->elementSize) {
                case 4:
                    return unpack('G', pack('g', $value))[1];
                case 8:
                    return unpack('E', pack('e', $value))[1];
            }
        } else {
            throw new LogicException("Can't flip this value kind");
        }
    }

    public function offsetGet($offset): mixed {
        if (!is_int($offset) || $offset < 0 || $offset >= $this->length) {
            return null;
        }

        return $this->view[$offset];
    }

    public function offsetSet($offset, $value): void {
        if (!is_int($offset) || $offset < 0 || $offset >= $this->length) {
            return;
        }

        $this->view[$offset] = $value;
    }

    public function offsetUnset($offset): void {
        throw new \LogicException("Can't unset values on a memory range");
    }

    public function offsetExists($offset): bool {
        return $offset >= 0 && $offset < $this->length;
    }

    public function count(): int {
        return $this->length;
    }

    public function getIterator(): \Generator {
        foreach ($this->view as $val) {
            yield $val->cdata;
        }
    }
}
