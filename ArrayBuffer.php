<?php
namespace Charm;

use FFI;
use Serializable;
use JsonSerializable;
use Countable;
use Charm\ArrayBuffer\View;

/**
 * Represents a memory buffer area, where the same underlying data can be manipulated via
 * an array of strongly typed unsigned integers, signed integers or floats.
 *
 * ```
 * $buffer = new ArrayBuffer(1024);
 * $uint8Array = $buffer->getUInt8View();
 * $uint8Array[0] = 256;
 * echo $uint8Array[0]; // outputs 0 because 256 overflows the uint8
 * ```
 *
 * @property View $uInt8   The underlying buffer as unsigned 8 bit integers
 * @property View $uInt16  The underlying buffer as unsigned 16 bit integers
 * @property View $uInt32  The underlying buffer as unsigned 32 bit integers
 * @property View $uInt64  The underlying buffer as unsigned 64 bit integers
 * @property View $int8    The underlying buffer as signed 8 bit integers
 * @property View $int16   The underlying buffer as signed 16 bit integers
 * @property View $int32   The underlying buffer as signed 32 bit integers
 * @property View $int64   The underlying buffer as signed 64 bit integers
 * @property View $float32 The underlying buffer as 32 bit floats
 * @property View $float64 The underlying buffer as 64 bit floats
 */
class ArrayBuffer implements Serializable, JsonSerializable, Countable {

    private $typeName;
    private $type;
    private $length;
    private $buffer;
    private $viewCache = [];

    /**
     * @param $byteLength How many bytes to allocate for this buffer. Will be rounded up to accommodate for the type specified.
     * @param $type The base data type (default is FFI::type('char'))
     */
    public function __construct(int $byteLength) {
        if ($byteLength === 0) {
            throw new ArrayBuffer\LogicException("Can't allocate an empty memory buffer");
        }
        $this->typeName = 'uint8_t';
        $this->type = FFI::type($this->typeName);
        $size = FFI::sizeof($this->type);
        $this->length = ceil($byteLength / $size) | 0;
        $this->buffer = FFI::new(FFI::arrayType($this->type, [ $this->length ]));
    }

    /**
     * {@see Serializable::serialize()}
     */
    public function serialize() {
        return serialize([$this->typeName, $this->toString()]);
    }

    /**
     * {@see Serializable::unserialize()}
     */
    public function unserialize(string $data) {
        $data = unserialize($data);
        $this->typeName = $data[0];
        $this->type = FFI::type($this->typeName);
        $size = FFI::sizeof($this->type);
        $this->length = ceil(strlen($data[1]) / $size) | 0;
        $this->buffer = FFI::new(FFI::arrayType($this->type, [ $this->length ]));
    }

    /**
     * {@see JsonSerializable::jsonSerialize()}
     */
    public function jsonSerialize() {
        return $this->toString();
    }

    /**
     * {@see Countable::count()}
     */
    public function count() {
        return $this->length;
    }

    /**
     * Create an array buffer from a PHP string. Note that this is a COPY
     * of the PHP string.
     */
    public static function fromString(string $data) {
        $buffer = new self(strlen($data));
        FFI::memcpy($buffer->buffer, $data, $buffer->length);
        return $buffer;
    }

    /**
     * Return the memory buffer as a string
     */
    public function toString() {
        return FFI::string($this->buffer, $this->length);
    }

    /**
     * Return a larger or smaller copy of the ArrayBuffer. The size will be rounded up to accommodate
     * for the underlying array type.
     */
    public function resize(int $byteLength): ArrayBuffer {
        $buffer = new self($byteLength);
        FFI::memcpy($buffer->buffer, $this->buffer, min($buffer->getByteLength(), $this->getByteLength()));
        return $buffer;
    }

    /**
     * Create a type which can be used with {@see ArrayBuffer::getStructView()} to parse complex binary
     * formats from the array buffer.
     *
     * @param array<string, string> $typeDef An array of [ name => type ] pairs.
     */
    public function createStruct(array $typeDef): FFI\CType {
        $type = "struct {\n";
        foreach ($typeDef as $key => $typeName) {
            $type .= $this->translateTypeName($typeName)." $key;\n";
        }
        $type .= "}";
        $cType = FFI::type($type);
        return $cType;
    }

    /**
     * Get a view of the underlying buffer with a custom data structure. This can be used to perform
     * efficient binary format decoding and encoding.
     *
     * @param FFI\CType $type A type created by {@see self::createStruct()} or {@see FFI::type()}
     * @param int $offset The byte offset
     * @return FFI\CData
     */
    public function getStructView(FFI\CType $type, int $offset=0): FFI\CData {
        $size = FFI::sizeof($type);
        if ($offset < 0) {
            throw new ArrayBuffer\OutOfBoundsException("Buffer access out of bounds");
        }
        if ($offset + $size > $this->getByteLength()) {
            throw new ArrayBuffer\OutOfBoundsException("Buffer access out of bounds");
        }

        $ptr = FFI::addr($this->buffer);
        $cast = FFI::cast('void *', $ptr[0] + $offset);
        return FFI::cast($type, $cast);
    }

    /**
     * Get the byte size of the buffer
     */
    public function getByteLength(): int {
        return FFI::sizeof($this->buffer);
    }

    public function getView($type): View {
        if (is_string($type)) {
            $type = $this->translateTypeName($type);
            $type = FFI::type($type);
        } elseif (!($type instanceof FFI\CType)) {
            throw new TypeError("Expecting a string or a FFI\CType for argument #1");
        }

        return new View($this->buffer, $type);
    }

    /**
     * Get an array of char which map to the underlying data buffer.
     *
     * @return array<char>
     */
    public function getCharView(): View {
        return $this->getView('char');
    }

    /**
     * Get an array of unsigned 8 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getUInt8View(): View {
        return $this->getView('uint8_t');
        return FFI::cast(FFI::arrayType(FFI::type('uint8_t'), [ $this->getByteLength() ]), $this->buffer);
    }

    /**
     * Get an array of unsigned 16 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getUInt16View(): View {
        return $this->getView('uint16_t');
        return FFI::cast(FFI::arrayType(FFI::type('uint16_t'), [ $this->getByteLength() >> 1]), $this->buffer);
    }

    /**
     * Get an array of 32 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getUInt32View(): View {
        return $this->getView('uint32_t');
        return FFI::cast(FFI::arrayType(FFI::type('uint32_t'), [ $this->getByteLength() >> 2]), $this->buffer);
    }

    /**
     * Get an array of unsigned 64 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getUInt64View(): View {
        return $this->getView('uint64_t');
        return FFI::cast(FFI::arrayType(FFI::type('uint64_t'), [ $this->getByteLength() >> 3]), $this->buffer);
    }

    /**
     * Get an array of 8 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getInt8View(): View {
        return $this->getView('int8_t');
        return FFI::cast(FFI::arrayType(FFI::type('int8_t'), [ $this->getByteLength() ]), $this->buffer);
    }

    /**
     * Get an array of 16 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getInt16View(): View {
        return $this->getView('int16_t');
        return FFI::cast(FFI::arrayType(FFI::type('int16_t'), [ $this->getByteLength() >> 1]), $this->buffer);
    }

    /**
     * Get an array of signed 32 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getInt32View(): View {
        return $this->getView('int32_t');
        return FFI::cast(FFI::arrayType(FFI::type('int32_t'), [ $this->getByteLength() >> 2]), $this->buffer);
    }

    /**
     * Get an array of signed 64 bit integers which map to the underlying data buffer.
     *
     * @return array<int>
     */
    public function getInt64View(): View {
        return $this->getView('int64_t');
        return FFI::cast(FFI::arrayType(FFI::type('int64_t'), [ $this->getByteLength() >> 3]), $this->buffer);
    }

    /**
     * Get an array of 32 bit float which map to the underlying data buffer.
     *
     * @return array<float>
     */
    public function getFloat32View(): View {
        return $this->getView('float');
        return FFI::cast(FFI::arrayType(FFI::type('float'), [ $this->getByteLength() >> 2]), $this->buffer);
    }

    /**
     * Get an array of 64 bit float which map to the underlying data buffer.
     *
     * @return array<float>
     */
    public function getFloat64View(): View {
        return $this->getView('double');
        return FFI::cast(FFI::arrayType(FFI::type('double'), [ $this->getByteLength() >> 3]), $this->buffer);
    }

    /**
     * Overload casting the structure to string
     */
    public function __toString() {
        return $this->toString();
    }

    /**
     * Magic method which enable accessing views as properties of this class.
     */
    public function __get(string $type) {
        if (isset($this->viewCache[$type])) {
            return $this->viewCache[$type];
        }
        if (method_exists($this, $fn = 'get'.ucfirst($type).'View')) {
            return $this->viewCache[$type] = $this->$fn();
        }
        return null;
    }

    /**
     * Magic method to simplify the look of var_dump()
     */
    public function __debugInfo() {
        return [
            'typeName' => $this->typeName,
            'buffer' => '...'.($this->length * FFI::sizeof($this->type)).' bytes...',
        ];
    }

    /**
     * Translate type names. May be useful to ensure consistency and compatability across different
     * hardware platforms and versions of PHP.
     *
     * @param string $type
     * @return string
     */
    private function translateTypeName(string $type): string {
        switch ($type) {
            case 'uint8': return 'uint8_t';
            case 'uint16': return 'uint16_t';
            case 'uint32': return 'uint32_t';
            case 'uint64': return 'uint64_t';
            case 'int8' : return 'int8_t';
            case 'int16' : return 'int16_t';
            case 'int32' : return 'int32_t';
            case 'int64' : return 'int64_t';
            case 'float32' :
                if (FFI::sizeof(FFI::type('float')) !== 4) {
                    throw new ArrayBuffer\NotImplementedException('float32 type is not available, and the float type is not 32 bit');
                }
                return 'float';
            case 'float64' :
                if (FFI::sizeof(FFI::type('double')) !== 8) {
                    throw new ArrayBuffer\NotImplementedException('float32 type is not available, and the double type is not 64 bit');
                }
                return 'float';
        }
        return $type;
    }
}
