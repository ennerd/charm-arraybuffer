<?php
use PHPUnit\Framework\TestCase;
use Charm\ArrayBuffer;

/**
 * @covers ArrayBuffer
 */
final class ArrayBufferTest extends TestCase {

    public function test_construct() {
        $range = new ArrayBuffer(4096);
        $this->assertInstanceOf(ArrayBuffer::class, $range);
    }

    public function test_too_small() {
        $this->expectException(ArrayBuffer\LogicException::class);
        $b = new ArrayBuffer(0);
    }

    public function test_little_too_small() {
        $b = new ArrayBuffer(1);
        $this->assertEquals(null, $b->uint64[0]);
        $this->assertEquals(null, $b->uint32[0]);
        $this->assertEquals(null, $b->uint16[0]);
        $this->assertEquals(null, $b->float32[0]);
        $this->assertEquals(null, $b->float64[0]);
    }

    public function test_endianness() {
        $b = new ArrayBuffer(8);
        $b->uint8[0] = 0xE1;
        $b->uint8[1] = 0xE2;
        $b->uint8[2] = 0xE2;
        $b->uint8[3] = 0xE1;
        $b->uint8[4] = 0xE1;
        $b->uint8[5] = 0xE2;
        $b->uint8[6] = 0xE2;
        $b->uint8[7] = 0xE1;

/*
    PHP 8.1 made this not work
        $this->assertEquals($b->uint64->flip($b->uint64[0]), $b->uint64[0]);
*/
        $this->assertEquals($b->uint32->flip($b->uint32[1]), $b->uint32[1]);
        $b->uint8[0] = 0xF1;
        $b->uint8[1] = 0xF1;
        $this->assertEquals($b->uint16->flip($b->uint16[0]), $b->uint16[0]);

        $b->uint8[0] = 0;
        $b->uint8[7] = 0;
        $b->uint8[1] = 32;
        $b->uint8[6] = 32;
        $b->uint8[2] = 122;
        $b->uint8[5] = 122;
        $b->uint8[3] = 196;
        $b->uint8[4] = 196;

        if ($b->float32[0] === -1000.5) {
            $this->assertEquals($b->float32[0], $b->float32->flip($b->float32[1]));
        } else {
            $this->assertEquals($b->float32[1], $b->float32->flip($b->float32[0]));
        }

    }

}
